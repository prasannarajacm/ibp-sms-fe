
$(document).on("click", ".add-chips" , function() {
  var dataID = $(this).attr("data-id");
  $('#myUsersModal').modal('show');
  $('#chips-update')[0].reset();
  $('#chips_id').val(dataID);
  console.log(dataID);
});

$(document).on("click", "#addChipsBtn" , function(e) {
  e.preventDefault();

  var chips_id = $('#chips_id').val();
  var chips_count = $('#chips_count').val();

  if(chips_count != "" && chips_count > 0){
    var update_chips = UpdateChipsCount(chips_id, chips_count);
    
  }else{
    toastr.error("Please provide valid chips count");
  }

});

$(document).on("click", ".pageida" , function(e) {
  e.preventDefault();
  var dataID = $(this).attr("data-id");
  users(dataID)
});

$(document).on("click", ".filterSubmitBtn" , function(e) {
  e.preventDefault();
  users()
});