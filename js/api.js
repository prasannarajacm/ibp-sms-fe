const base_url = "http://13.233.29.251/"; // test url
//const base_url = "http://127.0.0.1:8000/"; // local url

const users_api = base_url + "admin/user/";
const transaction_api = base_url + "admin/transactions/";
const games_api = base_url + "api/local/games/";
const chips_api = base_url + "api/local/chips/";

const listname_api = base_url + "api/list-numbers/";
const campaign_api = base_url + "api/campaign/";
const template_api = base_url + "api/templates/";
const get_template_api = base_url + "api/get-templates/";
const get_dashboard_api = base_url + "api/dashboard/";
// post


const allocateChips = base_url + "admin/user/update-player-wallet/";
const gameStatus = base_url + "api/local/games/set-availability/";
const UpdateChipsUrl = base_url + "admin/user/add-chips/";
const ExportUrl = base_url + "admin/user/export/";

$(function() {
  if (!localStorage.signedUser) {
    location.href = "login.html";
  }
});

$(".logoutbtn li ul li a").on("click", function(e) {
  e.preventDefault();
  localStorage.clear();
  location.href = "login.html";
});

function ajaxhelper(url, type, data) {
  return $.ajax({
    url: url,
    type: type,
    data: JSON.stringify(data),
    dataType: "json",
    headers: {
      Authorization: "Bearer " + localStorage.token
    },
    contentType: "application/json",
    success: function(response) {
      return response;
    },
    error: function(err) {
      console.log(err)
      error({
        message: err && err.responseJSON && err.responseJSON.message ?
          err.responseJSON.message : "something went wrong",
      });
    }
  });
}

async function list_names(pageNo=1) {

  var SearchFilter = $('#SearchFilter').val();
  var status = $('#status_filter').val();

  listname_api_url = listname_api+"?limit=10&page="+pageNo+"&search_filter="+SearchFilter+"&status="+status;
  const data = await ajaxhelper(listname_api_url, "GET");

  if (data.code == 900 && data.data) {
    let template = listname_template();
    data_items = data.data
    var html = Mustache.render(template, data_items);

    total_pages = data_items.total_pages;
    current_page = data_items.current_page;

    getinit_val = current_page-5;
    if(getinit_val < 1){
      init_page = 1;
    }else{
      init_page = getinit_val;
    }

    getstop_val = current_page+5;
    if(getstop_val <= total_pages){
      getstop_page = getstop_val;
    }else{
      getstop_page = total_pages;
    }

    var paginationDivId = ""
    for (var i = init_page; i <= getstop_page; i++) {
      paginationDivId += '<li class="page-item"><a class="page-link pageida" href="#" data-id="'+i+'">'+i+'</a></li>'
    }
    $("#paginationDivId").html(paginationDivId);

    $("#listnames_render").html(html);
    $(".loader").addClass("hide");
    $(".table-responsive").removeClass("hide");
  } else {
    error(data);
  }
}


async function templates_lists(pageNo=1) {

  var SearchFilter = $('#SearchFilter').val();
  var status = $('#status_filter').val();

  template_api_url = template_api+"?limit=10&page="+pageNo+"&search_filter="+SearchFilter+"&status="+status;
  const data = await ajaxhelper(template_api_url, "GET");

  if (data.code == 900 && data.data) {
    let template = template_lists();
    data_items = data.data
    var html = Mustache.render(template, data_items);

    total_pages = data_items.total_pages;
    current_page = data_items.current_page;

    getinit_val = current_page-5;
    if(getinit_val < 1){
      init_page = 1;
    }else{
      init_page = getinit_val;
    }

    getstop_val = current_page+5;
    if(getstop_val <= total_pages){
      getstop_page = getstop_val;
    }else{
      getstop_page = total_pages;
    }

    var paginationDivId = ""
    for (var i = init_page; i <= getstop_page; i++) {
      paginationDivId += '<li class="page-item"><a class="page-link pageida" href="#" data-id="'+i+'">'+i+'</a></li>'
    }
    $("#paginationDivId").html(paginationDivId);

    $("#template_lists_render").html(html);
    $(".loader").addClass("hide");
    $(".table-responsive").removeClass("hide");
  } else {
    error(data);
  }
}

async function dashboard_lists(pageNo=1) {

  var SearchFilter = $('#SearchFilter').val();
  var status = $('#status_filter').val();

  template_api_url = get_dashboard_api+"?limit=10&page="+pageNo+"&search_filter="+SearchFilter+"&status="+status;
  const data = await ajaxhelper(template_api_url, "GET");

  if (data.code == 900 && data.data) {
    let template = template_lists();
    data_items = data.data

    $("#list_name_count").html(data_items.lisname_count);
    $("#template_count").html(data_items.templates_count);
    $("#campaign_count").html(data_items.campaign_count);


  } else {
    error(data);
  }
}

async function download_fn_get() {

  alert(1);
}

// listname api
async function listname_fn_post() {

   var is_name = 1;
   var is_file = 1;

  let file = false;
  if ($("#modal-user-settings #csv_file")[0].files[0]) {
    file = $("#modal-user-settings #csv_file")[0].files[0];
  }

  var list_name = $("#modal-user-settings #list_name").val();

  if (!list_name){
    toastr.error('Error', 'Please enter the list name');
    is_name = 0;
  }

  if (!file){
    toastr.error('Error', 'Please choose the file');
    is_file = 0;
  }

  if(is_name == 1 && is_file == 1){
      var form_data = new FormData($('#list-form')[0]);
      $('.preloader').show();
      $('.preloader').css('display', 'block');
      $.ajax({
          type:'POST',
          url:listname_api,
          processData: false,
          contentType: false,
          async: false,
          cache: false,
          data : form_data,
          success: function(response){
                if(response.code == 900){
                    toastr.success('Success', 'Import Successfully');
                    list_names();
                    $("#modal-user-settings").modal("hide");
                    $('#list-form')[0].reset();
                }else{
                    toastr.error('Error', 'Something went wrong');
                }
                $('.preloader').hide();
                $('.preloader').css('display', 'none');
          }
      });
  }

}


async function template_fn_post() {

   var is_TID = 1;
   var is_TIC = 1;
   var is_SID = 1;

  var template_id = $("#modal-user-settings #template_id").val();
  var template_content = $("#modal-user-settings #template_content").val();
  var template_serial_id = $("#modal-user-settings #template_serial_id").val();
  var sender_ids = $("#modal-user-settings #sender_ids").val();

  if (!template_id){
    toastr.error('Error', 'Please enter the template id');
    is_TID = 0;
  }

  if (!template_content){
    toastr.error('Error', 'Please enter the template Content');
    is_TIC = 0;
  }

  if (!sender_ids){
    toastr.error('Error', 'Please enter the sender ids');
    is_SID = 0;
  }


  if(is_TID == 1 && is_TIC == 1 && is_SID == 1){

      var confirm_result = confirm("Declaration: The Template ID, Sender IDs, Template Content are similar to DLT platform");
      if(confirm_result){

          var method = "POST"
          var api_call = template_api
          console.log(template_serial_id)
          if (template_serial_id && template_serial_id > 0){
            var method = "PUT"
            var api_call = template_api+template_serial_id+'/'
          }

          var form_data = new FormData($('#template-form')[0]);
          $('.preloader').show();
          $('.preloader').css('display', 'block');
          $.ajax({
              type:method,
              url:api_call,
              processData: false,
              contentType: false,
              async: false,
              cache: false,
              data : form_data,
              success: function(response){
                    if(response.code == 900){
                        toastr.success('Success', response.message);
                        templates_lists();
                        $("#modal-user-settings").modal("hide");
                        $('#template-form')[0].reset();
                    }else{
                        toastr.error('Error', response.message);
                    }
                    $('.preloader').hide();
                    $('.preloader').css('display', 'none');
              }
          });
      }

  }

}

async function template_fn_edit(id) {

      const data = {};
      $('#template-form')[0].reset();
      const send_data = await ajaxhelper(template_api+id+'/', "get", data);
      if (send_data) {
        get_data = send_data.data
        $("#modal-user-settings #template_serial_id").val(id);
        $("#modal-user-settings #template_id").val(get_data.template_id);
        $("#modal-user-settings #template_content").val(get_data.template_content);
        $("#modal-user-settings #sender_ids").val(get_data.sender_ids);
        $('#modal-user-settings').modal('show');
        toastr.success('Success', 'Get Successfully');
        templates_lists();
      }

}

async function template_fn_add() {

      $('#template-form')[0].reset();

}

async function template_fn_delete(id) {

  const data = {};

  const send_data = await ajaxhelper(template_api+id+'/', "delete", data);
  if (send_data) {
    toastr.success('Success', 'Deleted Successfully');
    templates_lists();
  }

}

async function listname_fn_delete(id) {

  const data = {};

  const send_data = await ajaxhelper(listname_api+id+'/', "delete", data);
  if (send_data) {
    toastr.success('Success', 'Deleted Successfully');
    list_names();
  }

}

async function campaigns_lists(pageNo=1) {

  var SearchFilter = $('#SearchFilter').val();
  var status = $('#status_filter').val();

  campaign_api_url = campaign_api+"?limit=10&page="+pageNo+"&search_filter="+SearchFilter+"&status="+status;
  const data = await ajaxhelper(campaign_api_url, "GET");

  listname_api_url = listname_api;
  const list_data = await ajaxhelper(listname_api_url, "GET");

  template_api_url = template_api;
  const template_data = await ajaxhelper(template_api_url, "GET");

  if (list_data.code == 900 && list_data.data) {
    data_item_lists = list_data.data;
    $('#list_name_id').empty();
    $('#list_name_id').append($("<option value=''>Select list name</option>"));
    $.each(data_item_lists.items, function(key, value) {
         $('#list_name_id').append($("<option></option>").attr("value", value.id).text(value.list_name));
    });
  }

  if (template_data.code == 900 && template_data.data) {
    data_item_templates = template_data.data;
    $('#template_id').empty();
    $('#template_id').append($("<option value=''>Select Template ID</option>"));
    $.each(data_item_templates.items, function(tkey, tvalue) {
         $('#template_id').append($("<option></option>").attr("value", tvalue.template_id).text(tvalue.template_id));
    });
  }

  if (data.code == 900 && data.data) {
    let template = campaign_template();
    data_items = data.data
    var html = Mustache.render(template, data_items);

    total_pages = data_items.total_pages;
    current_page = data_items.current_page;

    getinit_val = current_page-5;
    if(getinit_val < 1){
      init_page = 1;
    }else{
      init_page = getinit_val;
    }

    getstop_val = current_page+5;
    if(getstop_val <= total_pages){
      getstop_page = getstop_val;
    }else{
      getstop_page = total_pages;
    }

    var paginationDivId = ""
    for (var i = init_page; i <= getstop_page; i++) {
      paginationDivId += '<li class="page-item"><a class="page-link pageida" href="#" data-id="'+i+'">'+i+'</a></li>'
    }
    $("#paginationDivId").html(paginationDivId);

    $("#campaings_render").html(html);
    $(".loader").addClass("hide");
    $(".table-responsive").removeClass("hide");
  } else {
    error(data);
  }
}

async function campaigns_fn_post() {

  var list_name_id = $("#modal-user-settings #list_name_id").val();
  var campaign_name = $("#modal-user-settings #campaign_name").val();
  var campaign_message = $("#modal-user-settings #campaign_message").val();
  var template_id = $("#modal-user-settings #template_id").val();
  var sender_id = $("#modal-user-settings #sender_id").val();

  if (list_name_id != "" && campaign_name != "" && campaign_message != "" && template_id != "" && sender_id != ""){
      const data = {
        list_name_id: list_name_id,
        campaign_name: campaign_name,
        campaign_message: campaign_message,
        template_id: template_id,
        sender_id: sender_id,
      };
      $('.preloader').show();
      const send_data = await ajaxhelper(campaign_api, "post", data);
      if (send_data) {
        toastr.success('Success', 'Send sms Successfully');
        campaigns_lists();
        $("#modal-user-settings").modal("hide");
        $('#campaign-form')[0].reset();
      }
      $('.preloader').hide();
  }else{
      toastr.error('Please Fill required fields', 'Error');
  }

}

async function template_content_get(template_id) {

  $("#modal-user-settings #campaign_message").val("");
  if (template_id != ""){
      $('.preloader').show();
      const send_data = await ajaxhelper(get_template_api+template_id+'/', "get");
      if (send_data) {
        if (send_data.code == 900){
            get_item_data = send_data.data
            template_content = get_item_data.template_content
            sender_ids = get_item_data.sender_ids
            $("#modal-user-settings #campaign_message").val(template_content);

            $('#sender_id').empty();
            $('#sender_id').append($("<option value=''>Select Sender ID</option>"));

            if (sender_ids){
                var sender_arr = sender_ids.split(",").map(function(item) {
                  return item.trim();
                });

                $.each(sender_arr, function(key, value) {
                     $('#sender_id').append($("<option></option>").attr("value", value).text(value));
                });
            }


        }
      }
      $('.preloader').hide();
  }

}

async function campaign_fn_view(id) {

  const data = {};

  const send_data = await ajaxhelper(campaign_api+id+'/', "get", data);
  if (send_data) {
     get_view_data = send_data.data
     $("#sid").html(get_view_data.id);
     $("#list_name").html(get_view_data.list_name);
     $("#campaign_name_v").html(get_view_data.campaign_name);
     $("#sender_id_v").html(get_view_data.sender_id);
     $("#template_id_v").html(get_view_data.template_id);
     $("#unique_campaign_id").html(get_view_data.unique_campaign_id);
     $("#total_numbers").html(get_view_data.total_numbers);
     $("#created_on").html(get_view_data.created_on);
     $("#campaign_message_v").html(get_view_data.campaign_message);

     var campaign_status = ""
     if(get_view_data.status == 0){
        campaign_status = '<b><p style="color: yellow;">Pending</p></b>';
     }else if(get_view_data.status == 1){
        campaign_status = '<b><p style="color: Green;">Completed</p></b>';
     }else{
        campaign_status = '<b><p style="color: red;">Failed</p></b>'
     }

     $("#status").html(campaign_status);
     $("#modal-view-settings").modal("show");
//    toastr.success('Success', 'Deleted Successfully');
//    list_names();
  }

}

async function users(pageNo=1) {

  var from_date = $('#from_date').val();
  var to_date = $('#to_date').val();
  var SearchFilter = $('#SearchFilter').val();

  var export_api = ExportUrl+"?date_from="+from_date+"&date_to="+to_date;
  $('#export-id-csv').attr('href', export_api);

  users_api_url = users_api+"?limit=5&page="+pageNo+"&date_from="+from_date+"&date_to="+to_date+"&search_filter="+SearchFilter;
  const data = await ajaxhelper(users_api_url, "GET");
  if (data.code == 900 && data.data) {
    let template = user_template();
    data_items = data.data
    var html = Mustache.render(template, data_items);

    total_pages = data_items.total_pages;
    current_page = data_items.current_page;

    getinit_val = current_page-5;
    if(getinit_val < 1){
      init_page = 1;
    }else{
      init_page = getinit_val;
    }

    getstop_val = current_page+5;
    if(getstop_val <= total_pages){
      getstop_page = getstop_val;  
    }else{
      getstop_page = total_pages;
    }

    var paginationDivId = ""
    for (var i = init_page; i <= getstop_page; i++) {
      paginationDivId += '<li class="page-item"><a class="page-link pageida" href="#" data-id="'+i+'">'+i+'</a></li>'
    }
    $("#paginationDivId").html(paginationDivId);
    
    $("#user_render").html(html);
    $(".loader").addClass("hide");
    $(".table-responsive").removeClass("hide");
  } else {
    error(data);
  }
}

async function transactions(pageNo=1) { 

  var SearchFilter = $('#SearchFilter').val();
  var status = $('#status_filter').val();

  transaction_api_url = transaction_api+"?limit=10&page="+pageNo+"&search_filter="+SearchFilter+"&status="+status;

  const data = await ajaxhelper(transaction_api_url, "GET");
  if (data.code == 900 && data.data) {
    let template = transaction_template();
    data_items = data.data
    var html = Mustache.render(template, data_items);

    total_pages = data_items.total_pages;
    current_page = data_items.current_page;

    getinit_val = current_page-5;
    if(getinit_val < 1){
      init_page = 1;
    }else{
      init_page = getinit_val;
    }

    getstop_val = current_page+5;
    if(getstop_val <= total_pages){
      getstop_page = getstop_val;  
    }else{
      getstop_page = total_pages;
    }

    var paginationDivId = ""
    for (var i = init_page; i <= getstop_page; i++) {
      paginationDivId += '<li class="page-item"><a class="page-link pageida" href="#" data-id="'+i+'">'+i+'</a></li>'
    }
    $("#paginationDivId").html(paginationDivId);

    $("#transaction_render").html(html);
    $(".loader").addClass("hide");
    $(".table-responsive").removeClass("hide");
  } else {
    error(data);
  }
}

async function games() {
  const data = await ajaxhelper(games_api, "GET");
  if (data.code == 900 && data.data) {
    let template = games_template();
    var html = Mustache.render(template, data);
    $("#games_render").html(html);
    $(".loader").addClass("hide");
    $(".table-responsive").removeClass("hide");
  } else {
    error(data);
  }
}

async function chips() {
  const data = await ajaxhelper(chips_api, "GET");
  if (data.code == 900 && data.data) {
    let template = chips_template();
    var html = Mustache.render(template, data);
    $("#chips_render").html(html);
    $(".loader").addClass("hide");
    $(".table-responsive").removeClass("hide");
  } else {
    error(data);
  }
}

async function addChips_post(id) {
  const data = {
    user_id: id,
    amount: $("#added_chips").val(),
    function: "debit"
  };
  const sendata = await ajaxhelper(allocateChips, "post", data);
  if (sendata) {
    users();
    $("#modal-edit-settings").modal("hide");
  }
}

async function UpdateChipsCount(id, count) {
  const data = {
    user_id: id,
    chips_value: count
  };
  const sendata = await ajaxhelper(UpdateChipsUrl, "post", data);
  if (sendata) {
    users();
    toastr.success("Chips count added");
    $('#myUsersModal').modal('hide');
    $('#chips-update')[0].reset();
  }
}

async function chipsopen(id) {
  $("#modal-edit-settings").modal("show");
  $("#btn_confirm").attr("data-id", id);
  // $("#added_chips").val(id);
}

async function activateGame_post(id) {
  const data = {
    availability: 1,
    games_list: [id]
  };
  const sendata = await ajaxhelper(gameStatus, "post", data);
  if (sendata) {
    games();
    $("#modal-activate").modal("hide");
  }
}

async function activateClick(id) {
  $("#modal-activate").modal("show");
  $("#activateGame").attr("data-id", id);
}

async function deactivateGame_post(id, availability) {
  const data = {
    availability: availability,
    games_list: [id]
  };
  const sendata = await ajaxhelper(gameStatus, "post", data);
  if (sendata) {
    games();
    toastr.success("Status changed");
    //$("#modal-deactivate").modal("hide");
  }
}

async function deactivateClick(id) {
  $("#modal-deactivate").modal("show");
  $("#deactivateGame").attr("data-id", id);
}
