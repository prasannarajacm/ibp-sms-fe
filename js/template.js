const listname_template = () => {
  return `
    {{#items}}
    <tr>
    <td>
      {{id}}
    </td>
    <td>
      {{list_name}}
    </td>
    <td>{{total_numbers}}</td>
    <td><a href="http://13.233.29.251/api/export-list-numbers/{{id}}/" download>download</a></td>
    <td>
      {{created_on}}
    </td>
    <td><button onclick="listname_fn_delete({{id}})">Delete</button></td>
  </tr>
    {{/items}}
  `;
};


const template_lists = () => {
  return `
    {{#items}}
    <tr>
    <td>
      {{id}}
    </td>
    <td>
      {{template_id}}
    </td>
    <td>{{template_content}}</td>
    <td>{{sender_ids}}</td>
    <td>{{created_on}}</td>
    <td>
        <button onclick="template_fn_edit({{id}})">Edit</button>
        <button onclick="template_fn_delete({{id}})">Delete</button>
    </td>
  </tr>
    {{/items}}
  `;
};


const campaign_template = () => {
  return `
    {{#items}}
    <tr>
    <td>
      {{id}}
    </td>
    <td>
      {{campaign_name}}
    </td>
    <td>
      {{template_id}}
    </td>
    <td>
      {{sender_id}}
    </td>
    <td>{{list_name}}</td>
    <td>{{total_numbers}}</td>
    <td>
        {{#status}}
          <b><p style="color: Green;">Completed</p></b>
        {{/status}}
        {{^status}}
           <b><p style="color: yellow;">Pending</p></b>
        {{/status}}
    </td>
    <td><button onclick="campaign_fn_view({{id}})">View</button></td>
  </tr>
    {{/items}}
  `;
};


const user_template = () => {
  return `
        {{#items}}
        <tr>
        <td class="text-center">{{id}}</td>
        <td>{{name}}</td>
        <td>{{email}}</td>
        <td>{{mobile_number}}</td>
        <td class="text-center">
          {{wallet}}
        </td>
        <td class="text-center">
          <button type="button" class="btn btn-primary btn-sm add-chips" data-id="{{id}}">
            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-plus" fill="currentColor" >
              <path fill-rule="evenodd" d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
            </svg>
          </button>
        </td>
      </tr>
          {{/items}}`;
};

const transaction_template = () => {
  return `
    {{#items}}
    <tr>
    <td>
      {{transaction_id}}
    </td>
    <td>
      {{user_name}}
    </td>
    <td>{{amount}}</td>
    <td>
      {{created_on}}
    </td>
    <td>
      {{transaction_status}}
    </td>
  </tr>
    {{/items}}
  `;
};

const games_template = () => {
  return `
    {{#data}}
    <tr>
      <td>
        {{server_game_id}}
      </td>
      <td>{{name}}</td>
      <td>
        {{brand}}
      </td>
      <td>

        {{#is_active}}
          ACTIVE
          <input type="text" id="isActiveId-{{server_game_id}}" style="display:none;" value="1">
        {{/is_active}}
        {{^is_active}}
          INACTIVE
          <input type="text" id="isActiveId-{{server_game_id}}" style="display:none;" value="0">
        {{/is_active}}

      </td>
      <td>
        <button class="change-status-game" data-id="{{server_game_id}}">Change</button>
      </td>
    </tr>
    {{/data}}
  `;
};

const chips_template = () => {
  return `
    {{#data}}
    <tr>
    <td>
      {{id}}
    </td>
    <td>
      {{currency}}
    </td>
    <td>
      {{chips_for_currency}}
    </td>
  </tr>
    {{/data}}
  `;
};
