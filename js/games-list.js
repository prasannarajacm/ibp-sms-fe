
$(document).on("click", ".change-status-game" , function() {
  var dataID = $(this).attr("data-id");
  var isActiveId  = $("#isActiveId-"+dataID).val();

  var isTrue = 0;
  if(isActiveId == 0){
    isTrue = 1;
    availability = 1;
  }else if(isActiveId == 1){
    isTrue = 1;
    availability = 0;
  }else{
    isTrue = 0;
    toastr.error("Something went wrong!");
  }

  if(isTrue == 1){
    deactivateGame_post(dataID, availability);
  }
  
});
